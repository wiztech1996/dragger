﻿using System;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Vector2 m_VtDown, m_VtUp;
    [SerializeField] private Rigidbody2D m_RbPlayer;
    private Vector2 m_VtForce => m_VtUp - m_VtDown;

    private void Awake()
    {
        m_RbPlayer = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
       
        if (Input.GetMouseButtonDown(0))
        {
            m_VtDown = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }

        if (Input.GetMouseButtonUp(0))
        {
            m_VtUp = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            m_RbPlayer.AddForce(m_VtForce, ForceMode2D.Impulse);
        }
    }
    
}